<?php
namespace Commerce\Cache;


use Nette\Caching\IStorage;
use Nette\Caching\Storages\DevNullStorage;
use Nette\Caching\Cache;
use Nette\DI\Container;
use Throwable;
use Tracy\Debugger;

class StaticCache implements CacheInterface {
	/** @var IStorage[] */
	private static $storage = [];

	/**
	 * @param IStorage $storage
	 */
	public static function setStorage( IStorage $storage ) {
		self::$storage[] = $storage;
	}

	public static function flushStorage():void
	{
		self::$storage = [];
	}

	/**
	 * @return IStorage
	 */
	public static function getStorage() : IStorage {
		if(self::$storage[0] instanceof IStorage){
			return self::$storage[0];
		}
		return new DevNullStorage();
	}

	public static function getStorages() : array {
		return self::$storage;
	}


	public static function load(string $group, string $key, callable $callback) {
		$cache = new Cache(static::getStorage(), $group);
		return $cache->load($key, $callback);
	}

	/**
	 * @deprecated Please use Cache::load(string $group, string $key, callable $callback)
	 */
	public static function check( $grp, $key ) {
		$cache = new Cache(static::getStorage(), $grp);
		$data = $cache->load($key);
		return ($data !== null) ? true : false;
	}
	/**
	 * @deprecated Please use Cache::load(string $group, string $key, callable $callback)
	 */
	public static function put( $grp, $key, $data ) {
		$cache = new Cache(static::getStorage(), $grp);
		$cache->save($key, $data);
	}
	/**
	 * @deprecated Please use Cache::load(string $group, string $key, callable $callback)
	 */
	public static function get( $grp, $key ) {
		$cache = new Cache(static::getStorage(), $grp);
		return $cache->load($key);
	}

	public static function flush( $grp, $mask = '*' ) {
		try {
			foreach (self::getStorages() as $storage){
				$cache = new Cache( $storage, $grp );
				$cache->clean( [
					Cache::NAMESPACES => [ $grp ]
				] );

			}
		} catch ( Throwable $e){
			Debugger::log($e->getMessage());
		}

	}
	public static function del( $grp, $mask = '*' ) {
		static::flush($grp,$mask);
	}

	/**
	 * @deprecated
	 */
	public static function cascade($src,$tgt) {
		//Debugger::log("cascade", Debugger::EXCEPTION);
		//bdump(debug_backtrace(), "cascade");
	}
}