<?php
namespace Commerce\Cache;

use Nette\Caching\IStorage;

interface CacheInterface {

	/**
	 * @param IStorage $storage
	 */
	public static function setStorage( IStorage $storage );

	public static function check($grp,$key);
	public static function put($grp,$key,$data);
	public static function get($grp,$key);
	public static function del($grp, $mask = '*');
	public static function flush(string $grp, $mask = '*');
	public static function load(string $group, string $key, callable $callback);

}