<?php

include __DIR__."/../vendor/autoload.php";

$cacheDir = __DIR__."/temp_cz/";
if(!file_exists($cacheDir)){
	\Nette\Utils\FileSystem::createDir($cacheDir);
}
Cache::setStorage(new \Nette\Caching\Storages\FileStorage($cacheDir));
Cache::setStorage(new \Nette\Caching\Storages\FileStorage(__DIR__."/temp/"));
$item = Cache::load("test","key", function (){
	return "čao";
});

Cache::flush("test");
dump($item);